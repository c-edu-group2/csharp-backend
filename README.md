# CSharp edu group2 / CSharp backend

CSharp backend library is a C# library for dealing with Physiotherapy time reservation.
The project has been done as a CSharp course exercise work in a small group.

Project includes:
- User register/login
- Authentication
- User roles for admin, employee and user (customer)
- Admin is able to add/edit/delete services, edit/delete users and add new employees
- Calendar for booking physiotherapy service
- Profile password change for all Roles
- Validation for password and email

Technologies used:
- C#
- pgAdmin4 PostgreSQL

## Installation

GitLab repository: https://gitlab.com/c-edu-group2/csharp-backend.git

## Usage

Also CSharp frontend git repository and pgAdmin4 database are needed for CSharp backend library usage.
Use the Visual Studio for CSharp backend C# environment. 

Do migrate for databases before start program:
- Update-Database -Migration 0
- Add-Migration InitialMigration
- Update-Database

## Known bugs and missing parts

Unfortunately time ran out at our course and some needed parts are missing from our application:
- retrieve a specific employee's calendar for the logged-in user and the user's own bookings
- calendar for a specific service
- authentication missing from calendar
- reservation end time was mentioned to be calculated from start time and service duration