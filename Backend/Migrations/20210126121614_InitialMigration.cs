﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace Backend.Migrations
{
    public partial class InitialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Appointment",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Title = table.Column<string>(type: "text", nullable: true),
                    StartDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    EndDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Appointment", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Services",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(type: "text", nullable: true),
                    Price = table.Column<int>(type: "integer", nullable: false),
                    Duration = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Services", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Fullname = table.Column<string>(type: "text", nullable: true),
                    Email = table.Column<string>(type: "text", nullable: true),
                    Password = table.Column<string>(type: "text", nullable: true),
                    Picture = table.Column<string>(type: "text", nullable: true),
                    Phonenumber = table.Column<string>(type: "text", nullable: true),
                    Skills = table.Column<string>(type: "text", nullable: true),
                    Infotext = table.Column<string>(type: "text", nullable: true),
                    Role = table.Column<string>(type: "text", nullable: true),
                    Token = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "Services",
                columns: new[] { "Id", "Duration", "Name", "Price" },
                values: new object[,]
                {
                    { 1, 30, "Massage 30 minutes", 35 },
                    { 2, 45, "Massage 45 minutes", 45 },
                    { 3, 60, "Massage 60 minutes", 60 },
                    { 4, 45, "Physiotherapy 45 minutes", 55 },
                    { 5, 60, "Physiotherapy 60 minutes", 65 },
                    { 6, 90, "Physiotherapy 90 minutes", 90 },
                    { 7, 45, "Hot stone 45 minutes", 40 },
                    { 8, 60, "Hot stone 60 minutes", 55 },
                    { 9, 90, "Hot stone 90 minutes", 80 }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "Email", "Fullname", "Infotext", "Password", "Phonenumber", "Picture", "Role", "Skills", "Token" },
                values: new object[,]
                {
                    { 1, "admin@physio.com", "Admin", "", "admintest", "", "", "Admin", "", null },
                    { 2, "a@physio.com", "EmployeeA", "I am physiotherapist and specialized to neck and back pains.", "employeea", "0401111111", "https://img.freepik.com/free-vector/cartoon-man-idea-think-creativity-design_24877-44706.jpg?size=338&ext=jpg", "Employee", "Physiotherapy", null },
                    { 3, "b@physio.com", "EmployeeB", "I have specified in sport massage and power massage.", "employeeb", "0401111112", "https://image.freepik.com/free-vector/cartoon-young-girl-icon_24908-23717.jpg", "Employee", "Basic massage, sport massage", null },
                    { 4, "c@physio.com", "EmployeeC", "I have spesified in hot stone massage and the power of aromatherapy", "employeec", "0401111113", "https://image.freepik.com/free-vector/cartoon-young-girl-icon_24908-23713.jpg", "Employee", "Hot stone massage, aromatherapist", null },
                    { 5, "a@gmail.com", "CustomerA", "", "customera", "", "", "User", "", null },
                    { 6, "b@gmail.com", "CustomerB", "", "customerb", "", "", "User", "", null }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Appointment");

            migrationBuilder.DropTable(
                name: "Services");

            migrationBuilder.DropTable(
                name: "Users");
        }
    }
}
