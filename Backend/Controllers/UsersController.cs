﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Backend.Models;
using Backend.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;

namespace Backend.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class UsersController : ControllerBase
    {
        public IUserRepo _userRepo;

        public UsersController(IUserRepo userRepo)
        {
            _userRepo = userRepo;
        }

        [AllowAnonymous]
        [HttpPost("login")]
        public IActionResult Authenticate([FromBody] AuthenticateRequest model)
        {
            var response = _userRepo.Authenticate(model.Email, model.Password);

            if (response == null)
                return BadRequest(new { message = "Username or password is incorrect" });

            return Ok(response);
        }

        [Authorize(Roles = Role.Admin)]
        [HttpGet]
        public IActionResult GetAll()
        {
            var users = _userRepo.GetAll();
            return Ok(users);
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            // only allow admins to access other user records
            var currentUserId = int.Parse(User.Identity.Name);
            if (id != currentUserId && !User.IsInRole(Role.Admin))
                return Forbid();

            var user = _userRepo.GetById(id);

            if (user == null)
                return NotFound();

            return Ok(user);
        }

        [HttpGet("email/{email}")]
        public IActionResult GetByEmail(string email)
        {
            var user = _userRepo.GetByEmail(email);

            if (user == null)
                return NotFound();

            return Ok(user);
        }

        [AllowAnonymous]
        [HttpGet("employee/{role}")]
        public IActionResult GetByRole(string role)
        {

            var users = _userRepo.GetByRole(role);

            return Ok(users);
        }

        [AllowAnonymous]
        [HttpGet("employee/{role}/{id}")]
        public IActionResult GetByRoleId(string role, int id)
        {

            var users = _userRepo.GetByRoleId(role, id);

            return Ok(users);
        }

        [AllowAnonymous]
        [HttpPost("register")]
        public IActionResult Post([FromBody] Users newUser)
        {
            var response = _userRepo.GetByEmail(newUser.Email);

            if (response == null)
            {
                _userRepo.AddUser(newUser);
                return Created(Request.Path + newUser.Id.ToString(), newUser);
            }
            return BadRequest(new { message = "Email already taken" });
        }

        [AllowAnonymous]
        [HttpPost("addNewEmployee")]
        public IActionResult AddNewEmployee([FromBody] Users newEmployee)
        {
            var response = _userRepo.GetByEmail(newEmployee.Email);

            if (response == null)
            {
                _userRepo.AddNewEmployee(newEmployee);
                return Created(Request.Path + newEmployee.Id.ToString(), newEmployee);
            }
            return BadRequest(new { message = "Email already taken" });
        }


        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] Users newUser)
        {
            var currentUserId = int.Parse(User.Identity.Name);
            if (id != currentUserId && !User.IsInRole(Role.Admin))
                return Forbid();

            Users userToUpdate = _userRepo.GetById(id);
            if (userToUpdate == null)
            {
                return NotFound();
            }
            _userRepo.UpdateUser(id, newUser);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var currentUserId = int.Parse(User.Identity.Name);
            if (id != currentUserId && !User.IsInRole(Role.Admin))
                return Forbid();

            Users userToDelete = _userRepo.GetById(id);
            if (userToDelete == null)
            {
                return NotFound();
            }
            _userRepo.DeleteUser(id);
            return Ok(userToDelete);
        }
    }
}


