﻿using Backend.Models;
using Backend.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace Backend.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class AppointmentController : ControllerBase
    {
        public IAppointmentRepo _appointmentRepo;

        public AppointmentController(IAppointmentRepo appointmentRepo)
        {
            _appointmentRepo = appointmentRepo;
        }

        [AllowAnonymous]
        [HttpGet]
        public IActionResult GetAppointments()
        {
            var appointment = _appointmentRepo.GetAppointments();
            return Ok(appointment);
        }

        [AllowAnonymous]
        [HttpGet("{id}")]
        public IActionResult GetAppointment(int id)
        {
            Appointment appointment = _appointmentRepo.GetAppointment(id);
            if (appointment == null)
            {
                return NotFound();
            }
            return Ok(appointment);
        }



        [AllowAnonymous]
        [HttpPost]
        public IActionResult Post([FromBody] Appointment newAppointment)
        {
            if (!TryValidateModel(newAppointment))
            {
                return BadRequest();
            }

            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            _appointmentRepo.AddAppointment(newAppointment);
            return Created(Request.Path + newAppointment.Id.ToString(), newAppointment);
        }


        [AllowAnonymous]
        [HttpDelete("{id}")]
        public IActionResult DeleteAppointment(int id)
        {
            var deleteAppointment = _appointmentRepo.GetAppointments()
                .FirstOrDefault(s => s.Id == id);

            if (deleteAppointment == null)
                return NotFound();

            _appointmentRepo.DeleteAppointment(deleteAppointment);

            return Ok(deleteAppointment);
        }
    }
}


