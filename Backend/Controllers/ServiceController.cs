﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Backend.Models;
using Backend.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;

namespace Backend.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class ServiceController : ControllerBase
    {
        public IServiceRepo _serviceRepo;

        public ServiceController(IServiceRepo serviceRepo)
        {
            _serviceRepo = serviceRepo;
        }

        [AllowAnonymous]
        [HttpGet]
        public IActionResult GetServices() =>
            Ok(_serviceRepo.GetServices());

        [AllowAnonymous]
        [HttpGet("{id}")]
        public IActionResult GetService(int id)
        {
            Service service = _serviceRepo.GetService(id);
            if (service == null)
            {
                return NotFound();
            }
            return Ok(service);
        }

        [Authorize(Roles = Role.Admin)]
        [HttpPost]
        public IActionResult Post([FromBody] Service newService)
        {
            if (!TryValidateModel(newService))
            {
                return BadRequest();
            }
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            _serviceRepo.AddService(newService);
            return Created(Request.Path + newService.Id.ToString(), newService);
        }

        [Authorize(Roles = Role.Admin)]
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] Service newService)
        {
            Service courseToUpdate = _serviceRepo.GetService(id);
            if (courseToUpdate == null)
            {
                return NotFound();
            }
            _serviceRepo.UpdateService(id, newService);
            return NoContent();
        }

        [Authorize(Roles = Role.Admin)]
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            Service serviceToDelete = _serviceRepo.GetService(id);
            if (serviceToDelete == null)
            {
                return NotFound();
            }
            _serviceRepo.DeleteService(id);
            return Ok(serviceToDelete);
        }
    }
}

