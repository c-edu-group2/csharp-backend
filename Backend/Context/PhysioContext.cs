﻿using Backend.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Context
{
    public class PhysioContext : DbContext
    {
        public DbSet<Users> Users { get; set; }
        public DbSet<Service> Services { get; set; }
        public DbSet<Appointment> Appointments { get; set; }

        public PhysioContext(DbContextOptions<PhysioContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Users>().HasData(
            new Users { Id = 1, Fullname = "Admin", Email = "admin@physio.com", Password = "admintest", Phonenumber = "", Picture = "", Infotext = "", Skills = "", Role = Role.Admin },
            new Users { Id = 2, Fullname = "EmployeeA", Email = "a@physio.com", Password = "employeea", Phonenumber = "0401111111", Picture = "https://img.freepik.com/free-vector/cartoon-man-idea-think-creativity-design_24877-44706.jpg?size=338&ext=jpg", Infotext = "I am physiotherapist and specialized to neck and back pains.", Skills = "Physiotherapy", Role = Role.Employee },
            new Users { Id = 3, Fullname = "EmployeeB", Email = "b@physio.com", Password = "employeeb", Phonenumber = "0401111112", Picture = "https://image.freepik.com/free-vector/cartoon-young-girl-icon_24908-23717.jpg", Infotext = "I have specified in sport massage and power massage.", Skills = "Basic massage, sport massage", Role = Role.Employee },
            new Users { Id = 4, Fullname = "EmployeeC", Email = "c@physio.com", Password = "employeec", Phonenumber = "0401111113", Picture = "https://image.freepik.com/free-vector/cartoon-young-girl-icon_24908-23713.jpg", Infotext = "I have spesified in hot stone massage and the power of aromatherapy", Skills = "Hot stone massage, aromatherapist", Role = Role.Employee },
            new Users { Id = 5, Fullname = "CustomerA", Email = "a@gmail.com", Password = "customera", Phonenumber = "", Picture = "", Infotext = "", Skills = "", Role = Role.User },
            new Users { Id = 6, Fullname = "CustomerB", Email = "b@gmail.com", Password = "customerb", Phonenumber = "", Picture = "", Infotext = "", Skills = "", Role = Role.User }
            );

            modelBuilder.Entity<Service>().HasData(
            new Service { Id = 1, Name = "Massage 30 minutes", Price = 35, Duration =30 },
            new Service { Id = 2, Name = "Massage 45 minutes", Price = 45, Duration = 45 },
            new Service { Id = 3, Name = "Massage 60 minutes", Price = 60, Duration = 60 },
            new Service { Id = 4, Name = "Physiotherapy 45 minutes", Price = 55, Duration = 45 },
            new Service { Id = 5, Name = "Physiotherapy 60 minutes", Price = 65, Duration = 60 },
            new Service { Id = 6, Name = "Physiotherapy 90 minutes", Price = 90, Duration = 90 },
            new Service { Id = 7, Name = "Hot stone 45 minutes", Price = 40, Duration = 45 },
            new Service { Id = 8, Name = "Hot stone 60 minutes", Price = 55, Duration = 60 },
            new Service { Id = 9, Name = "Hot stone 90 minutes", Price = 80, Duration = 90 }
            );

            modelBuilder.Entity<Appointment>().ToTable("Appointment");


        }
    }
}


