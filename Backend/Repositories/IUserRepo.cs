﻿using Backend.Models;
using System.Collections.Generic;

namespace Backend.Repositories
{
    public interface IUserRepo
    {
        IEnumerable<Users> GetAll();
        Users Authenticate(string Email, string Password);
        Users GetByEmail(string email);
        Users GetById(int id);
        List<Users> GetByRole(string role);
        Users GetByRoleId(string role, int id);
        void AddNewEmployee(Users newEmployee);
        void AddUser(Users newUser);
        void DeleteUser(int id);
        void UpdateUser(int id, Users newUser);
    }
}