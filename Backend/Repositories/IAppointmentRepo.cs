﻿using Backend.Models;
using System.Collections.Generic;

namespace Backend.Repositories
{
    public interface IAppointmentRepo
    {
        Appointment GetAppointment(int id);
        ICollection<Appointment> GetAppointments();
        List<Service> GetServices();
        List<Users> GetUsers();
        void AddAppointment(Appointment newAppointment);
        void DeleteAppointment(Appointment newAppointment);

    }
}