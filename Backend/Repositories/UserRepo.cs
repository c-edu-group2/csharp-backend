﻿using Backend.Context;
using Backend.Helpers;
using Backend.Models;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Backend.Repositories
{
    public class UserRepo : IUserRepo
    {
        private readonly PhysioContext _context;

        private readonly AppSettings _appSettings;
        public UserRepo(PhysioContext context, IOptions<AppSettings> appSettings)
        {
            _context = context;
            _appSettings = appSettings.Value;
        }

        public Users Authenticate(string Email, string Password)
        {
            var user = _context.Users.SingleOrDefault(x => x.Email == Email && x.Password == Password);

            // return null if user not found
            if (user == null)
                return null;

            // authentication successful so generate jwt token
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, user.Id.ToString()),
                    new Claim(ClaimTypes.Role, user.Role)
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            user.Token = tokenHandler.WriteToken(token);

            return user.WithoutPassword();
        }

        public IEnumerable<Users> GetAll()
        {
            return _context.Users;
        }

        public List<Users> GetByRole(string role) =>
            _context.Users.Where(c => c.Role == role).ToList();

        public Users GetByRoleId(string role, int id)
        {
            var user = _context.Users.SingleOrDefault(x => x.Role == role && x.Id == id);

            // return null if user not found
            if (user == null)
                return null;
            return user;
        }

        public Users GetById(int id)
        {
            var user = _context.Users.FirstOrDefault(x => x.Id == id);

            return user.WithoutPassword();
        }

        public Users GetByEmail(string email) =>
            _context.Users.SingleOrDefault(c => c.Email == email);

        //register new user:
        public void AddUser(Users newUser)
        {
            newUser.Id = newUser.Id;
            newUser.Fullname = newUser.Fullname;
            newUser.Email = newUser.Email;
            newUser.Password = newUser.Password;
            newUser.Role = Role.User;
            _context.Users.Add(newUser);
            _context.SaveChanges();
        }

        //add new user by Admin:
        public void AddNewEmployee(Users newEmployee)
        {
            newEmployee.Id = newEmployee.Id;
            newEmployee.Fullname = newEmployee.Fullname;
            newEmployee.Email = newEmployee.Email;
            newEmployee.Password = newEmployee.Password;
            newEmployee.Picture = newEmployee.Picture;
            newEmployee.Phonenumber = newEmployee.Phonenumber;
            newEmployee.Skills = newEmployee.Skills;
            newEmployee.Infotext = newEmployee.Infotext;
            newEmployee.Role = Role.Employee;
            _context.Users.Add(newEmployee);
            _context.SaveChanges();
        }

        public void UpdateUser(int id, Users newUser)
        {
            var users = GetById(id);
            users.Fullname = newUser.Fullname;
            users.Email = newUser.Email;
            users.Password = newUser.Password;
            users.Picture = newUser.Picture;
            users.Phonenumber = newUser.Phonenumber;
            users.Skills = newUser.Skills;
            users.Infotext = newUser.Infotext;
            users.Role = newUser.Role;
            _context.Users.Update(users);
            _context.SaveChanges();
        }
        public void DeleteUser(int id)
        {
            _context.Users.Remove(GetById(id));
            _context.SaveChanges();
        }
    }
}
