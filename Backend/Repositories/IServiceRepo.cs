﻿using Backend.Models;
using System.Collections.Generic;

namespace Backend.Repositories
{
    public interface IServiceRepo
    {
        Service GetService(int id);
        List<Service> GetServices();
        void AddService(Service newService);
        void DeleteService(int id);
        void UpdateService(int id, Service newService);
    }
}