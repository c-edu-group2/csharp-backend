﻿using Backend.Context;
using Backend.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Repositories
{
    public class AppointmentRepo : IAppointmentRepo
    {
        private readonly PhysioContext _context;

        public AppointmentRepo(PhysioContext context)
        {
            _context = context;
        }

        public ICollection<Appointment> GetAppointments() =>
            _context.Appointments.ToArray();

        public Appointment GetAppointment(int id) =>
           _context.Appointments.FirstOrDefault(c => c.Id == id);

        public List<Users> GetUsers() =>
            _context.Users.ToList();
        public List<Service> GetServices() =>
            _context.Services.ToList();


        public void AddAppointment(Appointment newAppointment)
        {
            _context.Appointments.Add(newAppointment);
            _context.SaveChanges();
        }

        public void DeleteAppointment(Appointment newAppointment)
        {
            _context.Appointments.Remove(newAppointment);
            _context.SaveChanges();
        }
    }
}

