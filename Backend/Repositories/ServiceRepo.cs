﻿using Backend.Context;
using Backend.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Repositories
{
    public class ServiceRepo : IServiceRepo
    {
        private readonly PhysioContext _context;

        public ServiceRepo(PhysioContext context)
        {
            _context = context;
        }

        public Service GetService(int id) =>
            _context.Services.FirstOrDefault(c => c.Id == id);

        public List<Service> GetServices() =>
            _context.Services.ToList();

        public void AddService(Service newService)
        {
            newService.Id = newService.Id;
            newService.Name = newService.Name;
            newService.Price = newService.Price;
            newService.Duration = newService.Duration;
            _context.Services.Add(newService);
            _context.SaveChanges();
        }

        public void UpdateService(int id, Service newService)
        {
            var service = GetService(id);
            service.Name = newService.Name;
            service.Price = newService.Price;
            service.Duration = newService.Duration;
            _context.Services.Update(service);
            _context.SaveChanges();
        }
        public void DeleteService(int id)
        {
            _context.Services.Remove(GetService(id));
            _context.SaveChanges();
        }
    }
}

