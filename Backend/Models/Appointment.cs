﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Backend.Models
{
    public partial class Appointment
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

    }
}
