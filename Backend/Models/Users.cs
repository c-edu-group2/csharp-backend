﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Backend.Models
{
    public partial class Users
    {

        public int Id { get; set; }
        public string Fullname { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Picture { get; set; }
        public string Phonenumber { get; set; }
        public string Skills { get; set; }
        public string Infotext { get; set; }
        public string Role { get; set; }
        public string Token { get; set; }


    }
}
