﻿using System;
using System.Collections.Generic;


namespace Backend.Models
{
    public partial class Service
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Price { get; set; }
        public int Duration { get; set; }


    }
}
